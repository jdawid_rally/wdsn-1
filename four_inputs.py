import numpy as np

def get_four():
	#1
	four_training_inputs = []

	four_training_inputs.append(np.array([
		0, 0, 0, 1, 0, 
		0, 0, 1, 1, 0, 
		0, 1, 0, 1, 0, 
		1, 1, 1, 1, 1, 
		0, 0, 0, 1, 0, 
		0, 0, 0, 1, 0 
	]))
	#2
	four_training_inputs.append(np.array([
		0, 0, 1, 1, 1,
		0, 0, 0, 0, 1,
		0, 0, 0, 1, 1,
		0, 0, 0, 0, 1,
		0, 0, 1, 1, 1,
		0, 0, 0, 0, 0
	]))
	#3
	four_training_inputs.append(np.array([
		0, 0, 0, 0, 0,
		0, 0, 0, 1, 0,
		0, 0, 1, 1, 0,
		0, 1, 1, 1, 1,
		0, 0, 0, 1, 0,
		0, 0, 0, 1, 0
	]))
	#4
	four_training_inputs.append(np.array([
		0, 0, 1, 0, 0,
		0, 1, 0, 1, 0,
		0, 0, 0, 1, 0,
		0, 0, 1, 0, 0,
		0, 1, 0, 0, 0,
		0, 1, 1, 1, 0
	]))
	#5
	four_training_inputs.append(np.array([
		0, 0, 1, 0, 0,
		0, 1, 1, 0, 0,
		1, 1, 1, 1, 0,
		0, 0, 1, 0, 0,
		0, 0, 0, 0, 0,
		0, 0, 0, 0, 0
	]))
	#6
	four_training_inputs.append(np.array([
		0, 0, 0, 0, 0,
		0, 1, 0, 0, 1,
		0, 0, 1, 0, 0,
		0, 1, 1, 0, 0,
		0, 0, 1, 0, 1,
		0, 0, 1, 0, 0
	]))
	#7
	four_training_inputs.append(np.array([
		0, 0, 0, 0, 0,
		0, 0, 0, 0, 0,
		0, 0, 0, 1, 0,
		0, 0, 1, 1, 0,
		0, 1, 1, 1, 1,
		0, 0, 0, 1, 0
	]))
	return four_training_inputs

def get_four_labels():
	four_labels = np.array([1, 0, 1, 0, 1, 0, 1])
	return four_labels