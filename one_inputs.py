import numpy as np

def get_one():
	one_training_inputs = []
	one_training_inputs.append(np.array([
		0, 0, 1, 0, 0,
		0, 1, 1, 0, 0,
		1, 0, 1, 0, 0,
		0, 0, 1, 0, 0,
		0, 0, 1, 0, 0,
		0, 0, 1, 0, 0
	]))
	one_training_inputs.append(np.array([
		0, 0, 0, 1, 0, 
		0, 0, 1, 1, 0, 
		0, 0, 0, 1, 0, 
		0, 0, 0, 1, 0, 
		0, 0, 0, 1, 0, 
		0, 0, 0, 1, 0 
	]))
	one_training_inputs.append(np.array([
		0, 0, 1, 0, 0,
		0, 0, 1, 0, 0,
		0, 0, 1, 0, 0,
		0, 0, 1, 0, 0,
		0, 0, 1, 0, 0,
		0, 0, 1, 0, 0
	]))
	one_training_inputs.append(np.array([
		0, 0, 1, 0, 0,
		0, 1, 1, 0, 0,
		0, 0, 1, 0, 0,
		0, 0, 1, 0, 0,
		0, 0, 0, 0, 0,
		0, 0, 0, 0, 0
	]))
	one_training_inputs.append(np.array([
		0, 0, 0, 0, 0,
		0, 0, 0, 0, 0,
		0, 0, 1, 0, 0,
		0, 1, 1, 0, 0,
		0, 0, 1, 0, 0,
		0, 0, 1, 0, 0
	]))
	
	one_training_inputs.append(np.array([
		0, 1, 0, 0, 0,
		0, 1, 0, 0, 0,
		0, 1, 0, 0, 0,
		0, 1, 0, 0, 0,
		0, 1, 0, 0, 0,
		0, 0, 0, 0, 0
	]))
	one_training_inputs.append(np.array([
		0, 0, 1, 0, 0,
		0, 0, 1, 0, 0,
		0, 0, 1, 0, 0,
		0, 0, 1, 0, 0,
		0, 0, 1, 0, 0,
		0, 0, 1, 0, 0
	]))
	one_training_inputs.append(np.array([
		0, 0, 0, 0, 0,
		0, 0, 0, 0, 1,
		0, 0, 0, 0, 1,
		0, 0, 0, 0, 1,
		0, 0, 0, 0, 1,
		0, 0, 0, 0, 0
	]))
	one_training_inputs.append(np.array([
		0, 0, 0, 1, 0,
		0, 0, 0, 1, 0,
		0, 0, 0, 1, 0,
		0, 0, 0, 1, 0,
		0, 0, 0, 1, 0,
		0, 0, 0, 0, 0
	]))
	one_training_inputs.append(np.array([
		0, 0, 0, 0, 0,
		0, 1, 0, 0, 0,
		0, 1, 0, 0, 0,
		0, 1, 0, 0, 0,
		0, 1, 0, 0, 0,
		0, 0, 0, 0, 0
	]))
	return one_training_inputs

def get_one_labels():
	one_labels = np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1])
	return one_labels