import numpy as np

def get_two():
	#1
	two_training_inputs = []

	two_training_inputs.append(np.array([
		0, 0, 1, 0, 0,
		0, 1, 0, 1, 0,
		0, 0, 0, 1, 0,
		0, 0, 1, 0, 0,
		0, 1, 0, 0, 0,
		0, 1, 1, 1, 0
	]))
	#2
	two_training_inputs.append(np.array([
		0, 1, 1, 1, 0,
		0, 0, 0, 1, 0,
		0, 1, 1, 1, 0,
		0, 1, 0, 0, 0,
		0, 1, 1, 1, 0,
		0, 0, 0, 0, 0
	]))
	#3
	two_training_inputs.append(np.array([
		0, 0, 1, 0, 0,
		0, 1, 0, 1, 0,
		0, 0, 0, 1, 0,
		0, 0, 1, 0, 0,
		0, 1, 0, 0, 0,
		1, 1, 1, 1, 0
	]))
	#4
	two_training_inputs.append(np.array([
		0, 0, 0, 1, 0,
		0, 0, 1, 0, 1,
		0, 0, 0, 0, 1,
		0, 0, 0, 1, 0,
		0, 0, 1, 0, 0,
		0, 1, 1, 1, 1
	]))
	#5
	two_training_inputs.append(np.array([
		0, 0, 1, 1, 0,
		0, 1, 0, 0, 1,
		0, 0, 0, 0, 1,
		0, 0, 0, 1, 0,
		0, 0, 1, 0, 0,
		0, 1, 1, 1, 1
	]))
	#6
	two_training_inputs.append(np.array([
		0, 0, 0, 0, 0,
		0, 1, 1, 1, 0,
		0, 0, 0, 1, 0,
		0, 1, 1, 1, 0,
		0, 1, 0, 0, 0,
		0, 1, 1, 1, 0
	]))
	#7
	two_training_inputs.append(np.array([
		0, 0, 0, 0, 0,
		0, 0, 1, 0, 0,
		0, 1, 0, 1, 0,
		0, 0, 1, 0, 0,
		0, 1, 1, 1, 0,
		0, 0, 0, 0, 0
	]))
	return two_training_inputs

def get_two_labels():
	two_labels = np.array([1, 1, 1, 1, 1, 1, 1])
	return two_labels