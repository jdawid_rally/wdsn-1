import numpy as np

def get_zero():
	#1
	zero_training_inputs = []

	zero_training_inputs.append(np.array([
		0, 0, 0, 0, 0,
		0, 1, 1, 1, 0,
		0, 1, 0, 1, 0,
		0, 1, 0, 1, 0,
		0, 1, 1, 1, 0,
		0, 0, 0, 0, 0
	]))
	#2
	zero_training_inputs.append(np.array([
		0, 1, 1, 0, 0,
		0, 0, 0, 1, 0,
		0, 1, 1, 1, 0,
		0, 0, 0, 1, 0,
		0, 0, 0, 1, 0,
		0, 1, 1, 0, 0
	]))
	#3
	zero_training_inputs.append(np.array([
		0, 0, 1, 0, 0,
		0, 1, 0, 1, 0,
		0, 1, 0, 1, 0,
		0, 0, 1, 0, 0,
		0, 0, 0, 0, 0,
		0, 0, 0, 0, 0
	]))
	#4
	zero_training_inputs.append(np.array([
		0, 0, 1, 0, 0,
		0, 1, 0, 1, 0,
		0, 0, 0, 1, 0,
		0, 0, 1, 0, 0,
		0, 1, 0, 0, 0,
		0, 1, 1, 1, 0
	]))
	#5
	zero_training_inputs.append(np.array([
		0, 1, 0, 0, 0,
		1, 0, 1, 0, 0,
		1, 0, 1, 0, 0,
		1, 0, 1, 0, 0,
		0, 1, 0, 0, 0,
		0, 0, 0, 0, 0
	]))
	#6
	zero_training_inputs.append(np.array([
		0, 0, 1, 1, 1,
		0, 0, 0, 0, 1,
		0, 0, 0, 1, 1,
		0, 0, 0, 0, 1,
		0, 0, 1, 1, 1,
		0, 0, 0, 0, 0
	]))
	#7
	zero_training_inputs.append(np.array([
		0, 0, 1, 1, 0,
		0, 1, 0, 0, 1,
		0, 1, 0, 0, 1,
		0, 1, 0, 0, 1,
		0, 1, 0, 0, 1,
		0, 0, 1, 1, 0
	]))
	return zero_training_inputs

def get_zero_labels():
	zero_labels = np.array([1, 0, 1, 0, 1, 0, 1])
	return zero_labels