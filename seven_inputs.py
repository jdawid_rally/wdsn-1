import numpy as np

def get_seven():
	#1
	seven_training_inputs = []

	seven_training_inputs.append(np.array([
		0, 1, 1, 1, 1,
		0, 0, 0, 1, 0,
		0, 0, 1, 0, 0,
		0, 1, 0, 0, 0,
		0, 0, 0, 0, 0,
		0, 0, 0, 0, 0
	]))
	#2
	seven_training_inputs.append(np.array([
		0, 1, 1, 0, 0,
		0, 0, 0, 1, 0,
		0, 1, 1, 1, 0,
		0, 0, 0, 1, 0,
		0, 0, 0, 1, 0,
		0, 1, 1, 0, 0
	]))
	#3
	seven_training_inputs.append(np.array([
		0, 1, 1, 1, 0,
		0, 0, 0, 1, 0,
		0, 0, 0, 1, 0,
		0, 0, 1, 0, 0,
		0, 0, 1, 0, 0,
		0, 0, 1, 0, 0
	]))
	#4
	seven_training_inputs.append(np.array([
		0, 0, 1, 0, 0,
		0, 1, 0, 1, 0,
		0, 0, 0, 1, 0,
		0, 0, 1, 0, 0,
		0, 1, 0, 0, 0,
		0, 1, 1, 1, 0
	]))
	#5
	seven_training_inputs.append(np.array([
		0, 0, 0, 0, 0,
		0, 1, 1, 1, 0,
		0, 0, 0, 1, 0,
		0, 0, 1, 0, 0,
		0, 0, 1, 0, 0,
		0, 0, 0, 0, 0
	]))
	#6
	seven_training_inputs.append(np.array([
		0, 0, 1, 1, 1,
		0, 0, 0, 0, 1,
		0, 0, 0, 1, 1,
		0, 0, 0, 0, 1,
		0, 0, 1, 1, 1,
		0, 0, 0, 0, 0
	]))
	#7
	seven_training_inputs.append(np.array([
		0, 0, 0, 0, 0,
		0, 0, 0, 0, 0,
		0, 1, 1, 1, 1,
		0, 0, 0, 1, 0,
		0, 0, 1, 0, 0,
		0, 1, 0, 0, 0
	]))
	return seven_training_inputs

def get_seven_labels():
	seven_labels = np.array([1, 0, 1, 0, 1, 0, 1])
	return seven_labels