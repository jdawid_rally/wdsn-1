import numpy as np

def get_eight():
	#1
	eight_training_inputs = []

	eight_training_inputs.append(np.array([
		0, 0, 1, 0, 0,
		0, 1, 0, 1, 0,
		0, 0, 1, 0, 0,
		0, 1, 0, 1, 0,
		0, 0, 1, 0, 0,
		0, 0, 0, 0, 0
	]))
	#2
	eight_training_inputs.append(np.array([
		0, 1, 1, 0, 0,
		0, 0, 0, 1, 0,
		0, 1, 1, 1, 0,
		0, 0, 0, 1, 0,
		0, 0, 0, 1, 0,
		0, 1, 1, 0, 0
	]))
	#3
	eight_training_inputs.append(np.array([
		0, 1, 1, 1, 0,
		0, 1, 0, 1, 0,
		0, 1, 1, 1, 0,
		0, 1, 0, 1, 0,
		0, 1, 1, 1, 0,
		0, 0, 0, 0, 0
	]))
	#4
	eight_training_inputs.append(np.array([
		0, 0, 1, 0, 0,
		0, 1, 0, 1, 0,
		0, 0, 0, 1, 0,
		0, 0, 1, 0, 0,
		0, 1, 0, 0, 0,
		0, 1, 1, 1, 0
	]))
	#5
	eight_training_inputs.append(np.array([
		0, 0, 0, 0, 0,
		0, 0, 1, 1, 1,
		0, 0, 1, 0, 1,
		0, 0, 1, 1, 1,
		0, 0, 1, 0, 1,
		0, 0, 1, 1, 1
	]))
	#6
	eight_training_inputs.append(np.array([
		0, 0, 1, 1, 1,
		0, 0, 0, 0, 1,
		0, 0, 0, 1, 1,
		0, 0, 0, 0, 1,
		0, 0, 1, 1, 1,
		0, 0, 0, 0, 0
	]))
	#7
	eight_training_inputs.append(np.array([
		0, 0, 0, 0, 0,
		1, 1, 1, 0, 0,
		1, 0, 1, 0, 0,
		1, 1, 1, 0, 0,
		1, 0, 1, 0, 0,
		1, 1, 1, 0, 0
	]))
	return eight_training_inputs

def get_eight_labels():
	eight_labels = np.array([1, 0, 1, 0, 1, 0, 1])
	return eight_labels