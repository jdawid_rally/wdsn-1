import tkinter as tk
from tkinter import *
from usage import *
import numpy as np

ROWS = 6
COLS = 5

learning = Learning()

tiles = [[None for _ in range(COLS)] for _ in range(ROWS)]

def callback(event):
    col_width = c.winfo_width()/COLS
    row_height = c.winfo_height()/ROWS
    col = int(event.x//col_width)
    row = int(event.y//row_height)
    if not tiles[row][col]:
        tiles[row][col] = c.create_rectangle(col*col_width, row*row_height, (col+1)*col_width, (row+1)*row_height, fill="blue")
    else:
        c.delete(tiles[row][col])
        tiles[row][col] = None
    v.set(learning.predict(parse_tiles_to_input(tiles)))

def parse_tiles_to_input(tiles):
	raw_inputs = []
	for row in tiles:
		for val in row:
			if val is not None:
				raw_inputs.append(1)
			else:
				raw_inputs.append(0)
	return np.array(raw_inputs)

root = tk.Tk()
c = tk.Canvas(root, width=500, height=600, borderwidth=5, background='yellow')

c.bind("<Button-1>", callback)
c.pack()

v = StringVar()
e = Label(root, textvar=v)
v.set("New Text!")
e.pack()

root.mainloop()