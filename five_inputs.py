import numpy as np

def get_five():
	#1
	five_training_inputs = []

	five_training_inputs.append(np.array([
		0, 1, 1, 1, 0, 
		0, 1, 0, 0, 0, 
		0, 1, 1, 0, 0, 
		0, 0, 0, 1, 0, 
		0, 1, 1, 0, 0, 
		0, 0, 0, 0, 0 
	]))
	#2
	five_training_inputs.append(np.array([
		0, 0, 1, 1, 1,
		0, 0, 0, 0, 1,
		0, 0, 0, 1, 1,
		0, 0, 0, 0, 1,
		0, 0, 1, 1, 1,
		0, 0, 0, 0, 0
	]))
	#3
	five_training_inputs.append(np.array([
		0, 0, 0, 0, 0,
		0, 0, 1, 1, 1,
		0, 0, 1, 0, 0,
		0, 0, 1, 1, 0,
		0, 0, 0, 0, 1,
		0, 0, 1, 1, 0
	]))
	#4
	five_training_inputs.append(np.array([
		0, 0, 1, 0, 0,
		0, 1, 1, 1, 0,
		0, 0, 0, 1, 0,
		0, 1, 1, 0, 1,
		0, 1, 0, 0, 1,
		0, 1, 1, 1, 0
	]))
	#5
	five_training_inputs.append(np.array([
		0, 0, 1, 0, 0,
		0, 1, 1, 0, 0,
		1, 1, 1, 1, 0,
		0, 0, 1, 0, 0,
		0, 0, 0, 0, 0,
		0, 0, 0, 0, 0
	]))
	#6
	five_training_inputs.append(np.array([
		0, 0, 0, 0, 0,
		0, 0, 0, 0, 0,
		0, 0, 0, 1, 0,
		0, 0, 1, 1, 0,
		0, 1, 1, 1, 1,
		0, 0, 0, 1, 0
	]))
	#7
	five_training_inputs.append(np.array([
		0, 0, 1, 1, 1,
		0, 0, 0, 0, 1,
		0, 0, 0, 1, 1,
		0, 0, 0, 0, 1,
		0, 0, 1, 1, 1,
		0, 0, 0, 0, 0
	]))
	five_training_inputs.append(np.array([
		0, 0, 1, 1, 1,
		0, 0, 0, 0, 1,
		0, 0, 0, 0, 1,
		0, 0, 0, 0, 1,
		0, 0, 1, 1, 1,
		0, 0, 0, 0, 0
	]))
	five_training_inputs.append(np.array([
		0, 1, 1, 0, 0,
		0, 0, 0, 1, 0,
		0, 1, 1, 1, 0,
		0, 0, 0, 1, 0,
		0, 0, 0, 1, 0,
		0, 1, 1, 0, 0
	]))
	return five_training_inputs

def get_five_labels():
	five_labels = np.array([1, 0, 1, 0, 0, 0, 0, 0, 0])
	return five_labels