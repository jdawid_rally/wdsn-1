import numpy as np

def get_nine():
	#1
	nine_training_inputs = []

	nine_training_inputs.append(np.array([
		0, 0, 1, 0, 0,
		0, 1, 0, 1, 0,
		0, 0, 1, 1, 0,
		0, 0, 0, 1, 0,
		0, 1, 1, 0, 0,
		0, 0, 0, 0, 0
	]))
	#2
	nine_training_inputs.append(np.array([
		0, 1, 1, 0, 0,
		0, 0, 0, 1, 0,
		0, 1, 1, 1, 0,
		0, 0, 0, 1, 0,
		0, 0, 0, 1, 0,
		0, 1, 1, 0, 0
	]))
	#3
	nine_training_inputs.append(np.array([
		0, 1, 1, 1, 0,
		0, 1, 0, 1, 0,
		0, 1, 1, 1, 0,
		0, 0, 0, 1, 0,
		0, 1, 1, 1, 0,
		0, 0, 0, 0, 0
	]))
	#4
	nine_training_inputs.append(np.array([
		0, 0, 1, 0, 0,
		0, 1, 0, 1, 0,
		0, 0, 0, 1, 0,
		0, 0, 1, 0, 0,
		0, 1, 0, 0, 0,
		0, 1, 1, 1, 0
	]))
	#5
	nine_training_inputs.append(np.array([
		0, 0, 0, 0, 0,
		0, 0, 1, 1, 1,
		0, 0, 1, 0, 1,
		0, 0, 1, 1, 1,
		0, 0, 0, 0, 1,
		0, 0, 1, 1, 1
	]))
	#6
	nine_training_inputs.append(np.array([
		0, 0, 1, 1, 1,
		0, 0, 0, 0, 1,
		0, 0, 0, 1, 1,
		0, 0, 0, 0, 1,
		0, 0, 1, 1, 1,
		0, 0, 0, 0, 0
	]))
	#7
	nine_training_inputs.append(np.array([
		0, 0, 0, 0, 0,
		1, 1, 1, 0, 0,
		1, 0, 1, 0, 0,
		1, 1, 1, 0, 0,
		0, 0, 1, 0, 0,
		1, 1, 1, 0, 0
	]))
	return nine_training_inputs

def get_nine_labels():
	nine_labels = np.array([1, 0, 1, 0, 1, 0, 1])
	return nine_labels