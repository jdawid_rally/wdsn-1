import numpy as np

def get_three():
	#1
	three_training_inputs = []

	three_training_inputs.append(np.array([
		0, 1, 1, 0, 0,
		0, 0, 0, 1, 0,
		0, 0, 0, 1, 0,
		0, 1, 1, 1, 0,
		0, 0, 0, 1, 0,
		0, 1, 1, 0, 0
	]))
	#2
	three_training_inputs.append(np.array([
		0, 1, 1, 1, 1,
		0, 0, 1, 0, 0,
		0, 0, 1, 0, 0,
		0, 0, 1, 0, 0,
		0, 0, 0, 1, 0,
		0, 0, 0, 1, 0
	]))
	#3
	three_training_inputs.append(np.array([
		0, 1, 1, 0, 0,
		0, 0, 0, 1, 0,
		0, 1, 1, 1, 0,
		0, 0, 0, 1, 0,
		0, 0, 0, 1, 0,
		0, 1, 1, 0, 0
	]))
	#4
	three_training_inputs.append(np.array([
		0, 0, 1, 0, 0,
		0, 1, 0, 1, 0,
		0, 0, 0, 1, 0,
		0, 0, 1, 0, 0,
		0, 1, 0, 0, 0,
		0, 1, 1, 1, 0
	]))
	#5
	three_training_inputs.append(np.array([
		0, 0, 1, 1, 1,
		0, 0, 0, 0, 1,
		0, 0, 0, 1, 1,
		0, 0, 0, 0, 1,
		0, 0, 1, 1, 1,
		0, 0, 0, 0, 0
	]))
	#6
	three_training_inputs.append(np.array([
		0, 0, 0, 0, 0,
		0, 0, 0, 0, 0,
		0, 0, 1, 0, 0,
		0, 1, 1, 0, 0,
		0, 0, 1, 0, 0,
		0, 0, 1, 0, 0
	]))
	#7
	three_training_inputs.append(np.array([
		0, 0, 1, 1, 1,
		0, 0, 0, 0, 1,
		0, 0, 0, 1, 1,
		0, 0, 0, 0, 1,
		0, 0, 1, 1, 1,
		0, 0, 0, 0, 0
	]))
	return three_training_inputs

def get_three_labels():
	three_labels = np.array([1, 0, 1, 0, 1, 0, 1])
	return three_labels