from perceptron import Perceptron
import numpy as np

from one_inputs import get_one_labels
from one_inputs import get_one

from two_inputs import get_two
from two_inputs import get_two_labels

from three_inputs import get_three
from three_inputs import get_three_labels

from four_inputs import get_four
from four_inputs import get_four_labels

from five_inputs import get_five
from five_inputs import get_five_labels

from six_inputs import get_six
from six_inputs import get_six_labels

from seven_inputs import get_seven
from seven_inputs import get_seven_labels

from eight_inputs import get_eight
from eight_inputs import get_eight_labels

from nine_inputs import get_nine
from nine_inputs import get_nine_labels

from zero_inputs import get_zero
from zero_inputs import get_zero_labels

class Learning():
	def __init__(self):
		self.perceptron1 = Perceptron(30, 1000)
		self.perceptron1.train(get_one(), get_one_labels())
		self.perceptron2 = Perceptron(30, 1000)
		self.perceptron2.train(get_two(), get_two_labels())
		self.perceptron3 = Perceptron(30, 1000)
		self.perceptron3.train(get_three(), get_three_labels())
		self.perceptron4 = Perceptron(30, 1000)
		self.perceptron4.train(get_four(), get_four_labels())
		self.perceptron5 = Perceptron(30, 1000)
		self.perceptron5.train(get_five(), get_five_labels())
		self.perceptron6 = Perceptron(30, 1000)
		self.perceptron6.train(get_six(), get_six_labels())
		self.perceptron7 = Perceptron(30, 1000)
		self.perceptron7.train(get_seven(), get_seven_labels())
		self.perceptron8 = Perceptron(30, 1000)
		self.perceptron8.train(get_eight(), get_eight_labels())
		self.perceptron9 = Perceptron(30, 1000)
		self.perceptron9.train(get_nine(), get_nine_labels())
		self.perceptron0 = Perceptron(30, 1000)
		self.perceptron0.train(get_zero(), get_zero_labels())

	def predict(self, inputs):
		predictions = []
		
		predictions.append(self.perceptron0.check_prediction(inputs));
		predictions.append(self.perceptron1.check_prediction(inputs));
		predictions.append(self.perceptron2.check_prediction(inputs));
		predictions.append(self.perceptron3.check_prediction(inputs));
		predictions.append(self.perceptron4.check_prediction(inputs));
		predictions.append(self.perceptron5.check_prediction(inputs));
		predictions.append(self.perceptron6.check_prediction(inputs));
		predictions.append(self.perceptron7.check_prediction(inputs));
		predictions.append(self.perceptron8.check_prediction(inputs));
		predictions.append(self.perceptron9.check_prediction(inputs));
		
		qualified = ''
		for index, val in enumerate(predictions):
			if val > 0.5:
				qualified = qualified+' '+str(index)+'('+str(val)+')';
		return qualified